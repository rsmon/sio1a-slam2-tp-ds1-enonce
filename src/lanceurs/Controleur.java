
package lanceurs;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;
import java.util.List;
import programmes.*;
 
/**
 *
 * @author rsmon
 */

public class Controleur {
   
        private List<String> listeProgrammes = new LinkedList<String>();
        private String       programmeSel    = null;
    
        public void init(){
        
            listeProgrammes.add("Exemple01"); 
            listeProgrammes.add("Exemple02"); 
            listeProgrammes.add("Exemple03"); 
            listeProgrammes.add("Exemple04"); 
            listeProgrammes.add("Exemple05"); 
            listeProgrammes.add("Programme01");
            listeProgrammes.add("Programme02");
            listeProgrammes.add("Programme03");
            listeProgrammes.add("Programme04"); 
            listeProgrammes.add("Programme05");
            listeProgrammes.add("Programme06");
            listeProgrammes.add("Programme07");
            listeProgrammes.add("Programme08"); 
            listeProgrammes.add("Programme09");
            listeProgrammes.add("Programme10"); 
        }
        
        public void executer(){
          
           if       ( programmeSel.equals( "Exemple01"   ) ) new Exemple01().executer();
           else if  ( programmeSel.equals( "Exemple02"   ) ) new Exemple02().executer();
           else if  ( programmeSel.equals( "Exemple03"   ) ) new Exemple03().executer();
           else if  ( programmeSel.equals( "Exemple04"   ) ) new Exemple04().executer();
           else if  ( programmeSel.equals( "Exemple05"   ) ) new Exemple05().executer();
           else if  ( programmeSel.equals( "Programme01" ) ) new Programme01().executer();
           else if  ( programmeSel.equals( "Programme02" ) ) new Programme02().executer();
           else if  ( programmeSel.equals( "Programme03" ) ) new Programme03().executer();
           else if  ( programmeSel.equals( "Programme04" ) ) new Programme04().executer();
           else if  ( programmeSel.equals( "Programme05" ) ) new Programme05().executer();
           else if  ( programmeSel.equals( "Programme06" ) ) new Programme06().executer();
           else if  ( programmeSel.equals( "Programme07" ) ) new Programme07().executer();
           else if  ( programmeSel.equals( "Programme08" ) ) new Programme08().executer();
           else if  ( programmeSel.equals( "Programme09" ) ) new Programme09().executer();
           else if  ( programmeSel.equals( "Programme10" ) ) new Programme10().executer();
        }
        
    
        //<editor-fold defaultstate="collapsed" desc="comment">
        
        public static final String PROP_LISTEPROGRAMMES = "listeProgrammes";
        
        public List<String> getListeProgrammes() {
            return listeProgrammes;
        }
        
        public void setListeProgrammes(List<String> listeProgrammes) {
            List<String> oldListeProgrammes = this.listeProgrammes;
            this.listeProgrammes = listeProgrammes;
            propertyChangeSupport.firePropertyChange(PROP_LISTEPROGRAMMES, oldListeProgrammes, listeProgrammes);
        }

               
    public static final String PROP_PROGRAMMESEL = "programmeSel";

    public String getProgrammeSel() {
        return programmeSel;
    }

    public void setProgrammeSel(String programmeSel) {
        String oldProgrammeSel = this.programmeSel;
        this.programmeSel = programmeSel;
        propertyChangeSupport.firePropertyChange(PROP_PROGRAMMESEL, oldProgrammeSel, programmeSel);
    }
        
        private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
        
        void effacerConsole() {
         
          try {
          
            Robot pressbot = new Robot();
       
            pressbot.mouseMove(400, 400);
            pressbot.mousePress(InputEvent.BUTTON3_MASK);
            pressbot.keyPress(KeyEvent.VK_CONTROL);
            pressbot.keyPress(KeyEvent.VK_L); 
            pressbot.keyRelease(KeyEvent.VK_L); 
            pressbot.keyRelease(KeyEvent.VK_CONTROL); 
            pressbot.keyPress(KeyEvent.VK_CLEAR);
         }  
         catch (AWTException ex) {}
         
     
         
                   
    }       
       //</editor-fold>


    
  
}
