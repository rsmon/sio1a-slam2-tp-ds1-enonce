
package gestionprojets;

import java.util.LinkedList;
import java.util.List;

public class Pole {
  
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private String codePole;
    private String nomPole;
    
    // Attribut navigationnel
    private List<Salarie> lesSalaries = new LinkedList();;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Pole() {}
    
    public Pole(String codePole, String nomPole) {
     
        this.codePole = codePole;
        this.nomPole  = nomPole;
     
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public String getCodePole() {
        return codePole;
    }
    
    public String getNomPole() {
        return nomPole;
    }
    
    public List<Salarie> getLesSalaries() {
        return lesSalaries;
    }
    
    public void setCodePole(String codePole) {
        this.codePole = codePole;
    }
    
    public void setNomPole(String nomPole) {
        this.nomPole = nomPole;
    }
    
    public void setLesSalaries(List<Salarie> lesSalaries) {
        this.lesSalaries = lesSalaries;
    }
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Méthodes Métiers">
    
    public int effectif(){
        
        return lesSalaries.size();
    }
    
    public int effectif(String pSexe){
        
        int nbSal=0;
        for (Salarie sal : lesSalaries ){
            
            if(pSexe.equals(sal.getSexe()) ) nbSal++;
            
        }
        return nbSal;
    }
    
    public Float moyenneSalaires(){
        
        Float moyenneSalaires=0f;
        Float sommeSalaires=0f;
        int   nbSalaires=0;
        
        for(Salarie sal : lesSalaries){
            
            sommeSalaires+=sal.getSalaire();
            nbSalaires++;
        }
        
        moyenneSalaires=(Float)sommeSalaires/nbSalaires;
        return moyenneSalaires;
        
    }
    
    public Float moyenneSalaires(String pSexe){
        
        Float moyenneSalaires=0f;
        Float sommeSalaires=0f;
        int   nbSalaires=0;
        
        for(Salarie sal : lesSalaries){
            
            if(pSexe.equals(sal.getSexe())) {
                
                sommeSalaires+=sal.getSalaire();
                nbSalaires++;
            }
        }
        moyenneSalaires=(Float)sommeSalaires/nbSalaires;
        
        return moyenneSalaires;
        
    }
    
    
    
    //</editor-fold>
}


