
package gestionprojets;

import java.util.LinkedList;
import java.util.List;
import utilitaires.UtilDate;
import static utilitaires.UtilDate.convChaineVersDate;


public class Entreprise {
    
   //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private static List<Salarie>     tousLesSalaries   = new LinkedList();
    private static List<Pole>        tousLesPoles      = new LinkedList();
    
    private static List<Client>      tousLesClients    = new LinkedList();
    private static List<Projet>      tousLesProjets    = new LinkedList();
     
    //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="Code Statique de création d'un jeu d'essais">
    
    static{
      
      Pole  pl1= new Pole("SLAM","Solutions Logicielles et Applications Métiers");
      Pole  pl2= new Pole("SISR","Solutions d'Infrastructures Systèmes et Réseaux");  
        
      tousLesPoles.add(pl1);tousLesPoles.add(pl2);
      
      Salarie s1 = new Salarie(101L, "Durant", "Pierre", "M", convChaineVersDate("12/05/1983"), 1830f );   
      tousLesSalaries.add( s1); lierSalariePole(s1, pl1);
      
      Salarie s2 = new Salarie(102L,"Martin", "Pierre", "M", convChaineVersDate("25/11/1991"), 2100f ); 
      tousLesSalaries.add( s2); lierSalariePole(s2, pl1);
      
      Salarie s3 = new Salarie(103L,"Lecoutre", "Thierry", "M", convChaineVersDate("05/08/1989"), 2320f ); 
      tousLesSalaries.add( s3); lierSalariePole(s3,pl2);
      
      Salarie s4 = new Salarie(104L,"Duchemin", "Fabienne", "F", convChaineVersDate("14/03/1992"), 1788f ); 
      tousLesSalaries.add( s4); lierSalariePole(s4,pl2);
       
      Salarie s5=  new Salarie(105L,"Duchateau", "Jacques", "M", convChaineVersDate("18/07/1992"), 1991f ) ;
      tousLesSalaries.add( s5);lierSalariePole(s5,pl1);
      
      Salarie s6= new Salarie(106L,"Lemortier", "Laurent", "M", convChaineVersDate("18/02/1980"), 1878f ) ;
      tousLesSalaries.add( s6);lierSalariePole(s6,pl1);
       
      Salarie s7=  new Salarie(107L,"Dessailles", "Sabine", "F", convChaineVersDate("23/03/1990"), 2450f );
      tousLesSalaries.add( s7);lierSalariePole(s7,pl2);
     
      Salarie s8=new Salarie(108L,"Bataille", "Boris", "M", convChaineVersDate("17/11/1987"), 1802f );
      tousLesSalaries.add( s8);lierSalariePole(s8,pl1);
      
      Salarie s9=new Salarie(109L,"Lerouge", "Laëtitia", "F", convChaineVersDate("09/10/1992"), 1946f ); 
      tousLesSalaries.add( s9);lierSalariePole(s9,pl1);

      Client c1= new Client(1001L,"Etbs Legrand","Lille");
      Client c2= new Client(1002L,"Centre Hospitalier Arras","Arras");
      Client c3= new Client(1003L,"Entreprise Legrain","Lille");
      Client c4= new Client(1004L,"Arcturus SA","Lens");
               
      tousLesClients.add(c1);tousLesClients.add(c2);tousLesClients.add(c3);tousLesClients.add(c4);
      
      Projet p1= new Projet("Alpha","Gestion De Production", convChaineVersDate("12/02/2015"),
                             convChaineVersDate("30/10/2015"),18150f
                           );
      tousLesProjets.add(p1);lierProjetClient(p1,c1);
      
      Projet p2= new Projet("Beta","Informatisation Blocs Opératoires", 
                             convChaineVersDate("20/11/2014"),convChaineVersDate("15/06/2015"),23400f
                           );
      tousLesProjets.add(p2);lierProjetClient(p2,c2);
      
      Projet p3= new Projet( "Delta","Interconnexion réseau MPLS",
                              convChaineVersDate("05/02/2015"),convChaineVersDate("15/03/2015"),6000f
                           );
      tousLesProjets.add(p3);lierProjetClient(p3,c3);
      
      
      Projet p4= new Projet( "Gamma","Développement Portail et Commerce Electronique",
                              convChaineVersDate("17/03/2015"),convChaineVersDate("05/04/2015"),4500f
                           );
      tousLesProjets.add(p4);lierProjetClient(p4,c3);
      
      
      Affectation a1,a2,a3,a4,a5,a6,a7,a8,a9,a10;
      
      a1=new Affectation(p1,s1);
      a1.setDateaff(UtilDate.convChaineVersDate("12/02/2015"));
      a1.setPilote(true);
      
      a5=new Affectation(p2,s1);
      a5.setDateaff(UtilDate.convChaineVersDate("20/11/2014"));
      a5.setPilote(true);
      
      a6=new Affectation(p1,s2);
      a6.setDateaff(UtilDate.convChaineVersDate("10/05/2015"));
      a6.setPilote(false);
      
      a2=new Affectation(p2,s3);
      a2.setDateaff(UtilDate.convChaineVersDate("05/12/2014"));
      a2.setPilote(false);

      a8=new Affectation(p3,s3);
      a8.setDateaff(UtilDate.convChaineVersDate("01/03/2015"));
      a8.setPilote(false);
      
      a4=new Affectation(p3,s4);
      a4.setDateaff(UtilDate.convChaineVersDate("06/02/2015"));
      a4.setPilote(false);
      
      a9=new Affectation(p4,s5);
      a9.setDateaff(UtilDate.convChaineVersDate("17/03/2015"));
      a9.setPilote(true);
      
      a3=new Affectation(p2,s7);
      a3.setDateaff(UtilDate.convChaineVersDate("17/03/2015"));
      a3.setPilote(false);
      
      a7=new Affectation(p3,s7);
      a7.setDateaff(UtilDate.convChaineVersDate("05/02/2015"));
      a7.setPilote(true);
      
      a10=new Affectation(p4,s9);
      a10.setDateaff(UtilDate.convChaineVersDate("20/03/2015"));
      a10.setPilote(false);
    }
    
        
   //<editor-fold defaultstate="collapsed" desc="Méthodes de Liaisons pour le jeu d'essais">
    
    private static void lierSalariePole(Salarie s1, Pole p1) {
        
        s1.setLePole(p1);
        p1.getLesSalaries().add(s1);
    }
    
    
    private static void lierProjetClient(Projet pProj, Client pCli) {
        
        pProj.setLeClient(pCli);
        pCli.getLesProjets().add(pProj);
    }
    //</editor-fold>
     
    
    //</editor-fold>
  
   //<editor-fold defaultstate="collapsed" desc="Méthodes d'accès aux données">
    
    
     public static List<Salarie> getTousLesSalaries() {
        return tousLesSalaries;
    }

    public static List<Pole> getTousLesPoles() {
        return tousLesPoles;
    }
    
    public static List<Client> getTousLesClients() {
        return tousLesClients;
    }

   

    public static List<Projet> getTousLesProjets() {
        return tousLesProjets;
    }
    
    
    
    
    public static Salarie      getSalarieDeNumero(Long pId){
        
        Salarie salarie=null;
        
        for(Salarie unSalarie: tousLesSalaries){
            
            if( unSalarie.getId().equals(pId) ){ 
                salarie=unSalarie;break;
            }
        }
        
        return salarie;
    }
    
    
    public static Client      getClientDeNumero(Long pNumCli){
        
        Client  cli=null;
        
        for(Client  unClient : tousLesClients){
             

            if( unClient.getNumcli().equals(pNumCli)){ 
                cli=unClient;break;
            }
        }
        
        return cli;
    }
    
    
    public static Pole   getPoleDeCode(String pCodePole){
        
        Pole pole=null;
        
        for(Pole unPole: tousLesPoles){
            
         if( unPole.getCodePole().equals(pCodePole)){
                pole=unPole;break;
         }
        }
        
        return pole;
    }

    
    public static Projet   getProjetDeCode(String pCodeProj){
        
        Projet projet=null;
        
        for(Projet unProjet: tousLesProjets){
            
         if( unProjet.getCodeProj().equals(pCodeProj)){
                projet=unProjet;break;
         }
        }
        
        return projet;
    }
    
    
   
    
    //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="Méthodes métiers">
    
    public static Float  moyenneSalaires(){
        
        Float totalSalaires=0f, moyenneSalaires=0f;
        int   nbSalaries=0;
        
        for (Salarie sal: tousLesSalaries){
            
            totalSalaires+=sal.getSalaire(); nbSalaries++;
            
        }
        moyenneSalaires=totalSalaires/nbSalaries;
        
        return moyenneSalaires;
    }
    
    public static Float  moyenneSalaires(String pSexe){
        
        Float totalSalaires=0f, moyenneSalaires=0f;
        int   nbSalaries=0;
        
        for (Salarie sal: tousLesSalaries){
            
            if( sal.getSexe().equals(pSexe)){
                
                totalSalaires+=sal.getSalaire(); nbSalaries++;
            }
        }
        moyenneSalaires=totalSalaires/nbSalaries;
        
        return moyenneSalaires;
    }
    
    public static int effectif(){  return tousLesSalaries.size();}
    
    public static int effectif(String pSexe){
        
        int nb=0;
        
        for (Salarie sal: tousLesSalaries){
            
            if( sal.getSexe().equals(pSexe)){ nb++; }
        }
        
        return nb;
    }
    
    
    public static int nombreDeClients(){
    
        
     // A vous d'ecrire cette méthode   
      return 0;
    }
    
    
    
    public static Float sommeDeTousLesDevisProjets(){
    
     // A vous d'ecrire cette méthode    
     return null;
    }
    
    
    public static Float sommeDeTousLesDevisProjetsSeTerminantEn( int pAnnee){
    
     // A vous d'ecrire cette méthode    
     return null;
    }
    
    
    //</editor-fold>
  
}


