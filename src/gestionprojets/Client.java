
package gestionprojets;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author rsmon
 */
public class Client {
    
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private Long   numcli;
    
    private String nomcli;
    
    
    private String adrcli;
    
    
    private List<Projet>   lesProjets= new LinkedList();
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Client() {
    }
    
    public Client(Long numcli, String nomcli, String adrcli) {
        this.numcli = numcli;
        this.nomcli = nomcli;
        this.adrcli = adrcli;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">

    public List<Projet> getLesProjets() {
        return lesProjets;
    }

    public void setLesProjets(List<Projet> lesProjets) {
        this.lesProjets = lesProjets;
    }
     
     
     public Long getNumcli() {
         return numcli;
     }
     
     public void setNumcli(Long numcli) {
         this.numcli = numcli;
     }
     
     
     public String getNomcli() {
         return nomcli;
     }
     
     public void setNomcli(String nomcli) {
         this.nomcli = nomcli;
     }
     
     
    public String getAdrcli() {
        return adrcli;
    }

    public void setAdrcli(String adrcli) {
        this.adrcli = adrcli;
    }

     
     
     
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Méthodes métiers">
    
    public void afficher(){
        
        // A vous d'écrire cette méthode
    }
    
    
    public Float sommeDevisProjets(){
    
      // A vous d'ecrire cette méthode  
      return null;
    }
    
    
    public Float sommeDevisProjetsSeTerminantEn( int pAnnee){
    
      // A vous d'ecrire cette méthode  
      return null;
    }
    
    //</editor-fold>
    
}
